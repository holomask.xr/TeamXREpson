package com.holomask.teamxr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.WifiParsedResult;
//import com.google.zxing.client.result.ResultParser;
//import com.google.zxing.client

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Toast;

public class ScannerView extends TXRBaseActivity {

    private CodeScanner mCodeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanner_view);
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ScannerView.this, result.getText(), Toast.LENGTH_SHORT).show();

                        // try to understand what of kind of QRCODE this is
                        if (result.getText().startsWith("WIFI:")) {
                            ParsedResult presult = com.holomask.teamxr.ResultParser.parseResult(result);
                            //Toast.makeText(ScannerView.this, presult.toString(), Toast.LENGTH_SHORT).show();
                            WifiManager wifiManager = (WifiManager) ScannerView.this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                            new WifiConfigManager(wifiManager,ScannerView.this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (WifiParsedResult) presult);
                        }
                        else {
                            // save the address in local storage
                            SharedPreferences sharedPreferences = getSharedPreferences("IDvalue", 0);
                            //SharedPreferences sharedPreferences = getPreferences("IDvalue", 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("com.holomask.teamxr.address", result.getText());
                            editor.commit();
                        }

                        Intent myInt = new Intent(ScannerView.this, StartUpActivity.class);
                        startActivity(myInt);
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}