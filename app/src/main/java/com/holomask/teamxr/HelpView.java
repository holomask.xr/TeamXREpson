package com.holomask.teamxr;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HelpView extends TXRBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_view);
    }

    public void closePressed(View view)
    {
        Intent myInt = new Intent(this, StartUpActivity.class);
        startActivity(myInt);
    }
}