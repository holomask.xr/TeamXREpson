package com.holomask.teamxr;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class StartUpActivity extends TXRBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);
        Timer timer = new Timer();
        final int FPS = 40;
        TimerTask updateBall = new UpdateBatteryTask();
        timer.scheduleAtFixedRate(updateBall, 3000, 2000);
        //getBatteryPercentage(this);
    }

    public void readQRCODE(View view) {
        // Do something in response to button click
        Intent i = new Intent(this,ScannerView.class);
        startActivity(i);
    }

    public void openBrowser(View view){
        // check if we have configured the glasses with an address:
        SharedPreferences sharedPreferences = getSharedPreferences("IDvalue", 0);
        String webAddress = sharedPreferences.getString("com.holomask.teamxr.address","NONEXISTENT");
        if(webAddress == "NONEXISTENT"){
            Toast.makeText(StartUpActivity.this, "Warning, application is not configured", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(StartUpActivity.this, "Configuration found!", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
    }

    public void openDocumentation(View view){
        //Toast.makeText(StartUpActivity.this, "NOT IMPLEMENTED!", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, HelpView.class);
        startActivity(i);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static int getBatteryPercentage(Context context) {

        if (Build.VERSION.SDK_INT >= 21) {

            BatteryManager bm = (BatteryManager) context.getSystemService(BATTERY_SERVICE);
            return bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

        } else {

            IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, iFilter);

            int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
            int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

            double batteryPct = level / (double) scale;

            return (int) (batteryPct * 100);
        }
    }

    class UpdateBatteryTask extends TimerTask {
        public void run() {
            final int  percentage = getBatteryPercentage(StartUpActivity.this);
            Log.i("TAG", "Battery..." + percentage);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView helloTextView = (TextView) findViewById(R.id.textBattery);
                    helloTextView.setText("Battery: " + percentage + "%");
                }
            });

        }
    }
}