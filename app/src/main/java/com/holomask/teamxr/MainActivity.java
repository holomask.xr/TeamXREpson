package com.holomask.teamxr;

import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Build;
import android.util.Log;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.widget.TextView;
import android.widget.Toast;
import android.webkit.WebView;
import android.view.View;
import android.webkit.WebViewClient;
import android.graphics.Bitmap;
import android.content.Intent;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.epson.moverio.btcontrol.DisplayControl;

import java.util.Timer;
import java.util.TimerTask;

import static com.holomask.teamxr.StartUpActivity.getBatteryPercentage;

public class MainActivity extends TXRBaseActivity implements AdvancedWebView.Listener, SensorEventListener {

    //private static final String TEST_PAGE_URL = "https://holomask.site:5000/moverio300?name=GlassesGOD&room=holomask";
    //private static final String TEST_PAGE_URL = "https://cozmo.github.io/jsQR/";
    private static final String TEST_PAGE_URL = "file:///android_asset/index.html";
    private AdvancedWebView mWebView;
    private Timer timer;
    private TimerTask updateBall;

    private final int TAPPED = 2;

    private DisplayControl mDisplayControl;
    private SensorManager mSensorManager;

    private boolean mIsMute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // EPSON SPECIFIC FOR DISPLAY MUTE
        mIsMute = false;
        mDisplayControl = new DisplayControl(this);



        setContentView(R.layout.activity_main);
        updateBall = new MainActivity.UpdateBatteryTask2();

        mWebView = (AdvancedWebView) findViewById(R.id.mainWebView);
        mWebView.setListener(this, this);
        //mWebView.setDesktopMode(false);
        mWebView.setGeolocationEnabled(false);
        mWebView.setMixedContentAllowed(true);
        mWebView.setCookiesEnabled(true);
        mWebView.setThirdPartyCookiesEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                Toast.makeText(MainActivity.this, "Finished loading", Toast.LENGTH_SHORT).show();
            }

        });
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                Toast.makeText(MainActivity.this, title, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.getResources());
                }
            }

        });
        mWebView.addHttpHeader("X-Requested-With", "");
        //mWebView.loadUrl(TEST_PAGE_URL);


        // retrieve the webpageaddress, glasses name and room
        // check if we have configured the glasses with an address:
        SharedPreferences sharedPreferences = getSharedPreferences("IDvalue", 0);
        String webAddress = sharedPreferences.getString("com.holomask.teamxr.address","NONEXISTENT");
        if(webAddress == "NONESISTE"){
            Toast.makeText(MainActivity.this, "Warning, application is not configured", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(MainActivity.this, "Configuration found!", Toast.LENGTH_SHORT).show();
            mWebView.loadUrl(webAddress);
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        setMute(false);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (mSensorManager != null) {
            Sensor tap = mSensorManager.getDefaultSensor(0x00002001);
            mSensorManager.registerListener(this, tap, SensorManager.SENSOR_DELAY_UI);
        }
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        setMute(false);

        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
        // ...
    }

    @Override
    public void onBackPressed() {
        if (!mWebView.onBackPressed()) { return; }
        // ...
        Log.i("TAG", "Back pressed ...");

        // stop the timer
        if(timer != null) {
            timer.cancel();
            timer = null;
        }

        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        mWebView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPageFinished(String url) {

        mWebView.setVisibility(View.VISIBLE);
        timer = new Timer();
        timer.scheduleAtFixedRate(updateBall, 3000, 2000);

    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        Toast.makeText(MainActivity.this, "onPageError(errorCode = "+errorCode+",  description = "+description+",  failingUrl = "+failingUrl+")", Toast.LENGTH_SHORT).show();
        Intent myInt = new Intent(MainActivity.this, StartUpActivity.class);
        startActivity(myInt);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        Toast.makeText(MainActivity.this, "onDownloadRequested(url = "+url+",  suggestedFilename = "+suggestedFilename+",  mimeType = "+mimeType+",  contentLength = "+contentLength+",  contentDisposition = "+contentDisposition+",  userAgent = "+userAgent+")", Toast.LENGTH_LONG).show();

		/*if (AdvancedWebView.handleDownload(this, url, suggestedFilename)) {
			// download successfully handled
		}
		else {
			// download couldn't be handled because user has disabled download manager app on the device
		}*/
    }

    @Override
    public void onExternalPageRequest(String url) {
        Toast.makeText(MainActivity.this, "onExternalPageRequest(url = "+url+")", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 0x00002001) {
            if (event.values[0] == TAPPED) {
                if (mIsMute) {
                    // Mute ON -> OFF
                    setMute(false);
                } else {
                    // Mute OFF -> ON
                    setMute(true);
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void setMute(boolean isMute) {
        mIsMute = isMute;
        mDisplayControl.setMute(isMute);
    }

    class UpdateBatteryTask2 extends TimerTask {
        public void run() {
            final int  percentage = getBatteryPercentage(MainActivity.this);
            Log.i("TAG", "Battery web ..." + percentage);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= 19) {
                        int displayStatus = 1;
                        if(mIsMute)
                            displayStatus = 0;

                        mWebView.evaluateJavascript("(function () {" +
                                "var btns = document.getElementsByClassName('statsArea')[0];" +
                                "btns.innerText = 'battery: ' +"+percentage+" ;" +
                                "btns.innerText += ', disp: ' +"+displayStatus+" ;" +
                                "  })();", null);
                    }
                    else {
                        mWebView.loadUrl("javascript:(function () {  })();");
                    }
                }
            });

        }
    }

}